This app displays 10 Points of Interests (POIs) in New York City!!

You can click on an icon or the name from list shown on the left
to get information about that POI. Or you can search for a POI by
name via the query box and clicking on the name.


This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).   

To access this app you can either clone the repository or download    
the zip file from In the project directory, you can run: https://bitbucket.org/kchrisj/fend7

Once you've copied the app to your computer, cd into the directory.

Once in the directory, you can:   
Create a production version of the app by running the command: npm run build. [Production info](https://reactjs.org/docs/optimizing-performance.html#use-the-production-build).   
Or create a development version by running npm start.

In the development mode, Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
