import React, { Component } from 'react';
import './App.css';
import axios from 'axios';
import MenuList  from './MenuList';

class App extends Component {

  componentDidMount() {

      window.gm_authFailure = () => {
        alert(
          "Hmmm. Somthing's not kosher with Google Maps... Please try again later."
        );
      };
      this.getPOIs()
  }

  state = {
    POIs: [],
    lat: 40.782864,
    lng: -73.965355,
    zoom: 12,
    myMap:'',
    infowindow: '',
    myMarkers: [],
    open: false,
    toggleBounce: '',
    toggleList: ''
  }

  //Getting data from thrid party API Foursquare
  getPOIs = () => {
    const endPoint = 'https://api.foursquare.com/v2/venues/explore?'
    const parameters = {
      client_id: "ENAKQVLBKHWVS2CBL4R2OWDQD2KKXL0DBP2CRWMV2HXABKP1",
      client_secret: "P2ILHJQFMBS2MHACMZU2EKZ1QI3LKNM11Z3HSC4QU1WHPZDI",
      query: 'sights',
      near: 'New York',
      v:'20182507',
      limit:10
    }

    //Using AXIOS (Promise based HTTP client for the browser and node.js) to get data from Foursquare
    axios.get(endPoint + new URLSearchParams(parameters))
    .then(response => {
      this.setState({POIs:response.data.response.groups[0].items}, this.renderMap())
    })
    .catch(error =>{
      console.log("ERROR" + error);
    })
    .catch(error => {
      alert("ERROR:" + error);
    })
  }

  // Render map and markers for each Point Of Interest
  renderMap = () => {
    loadJS('https://maps.googleapis.com/maps/api/js?key=AIzaSyDTwqpsaMgNr-bxhWowfQ8fTlZ_MPvErhI&callback=initMap')
    window.initMap = this.initMap
  }

  // Toggle function to show/hide sidebar
    toggleList = () => {
      this.setState({
         open: !this.state.open
      });
    }


  //function used to initialize map
  initMap = () => {
    const map = new window.google.maps.Map(document.getElementById('map'), {
      center:  {lat: this.state.lat, lng: this.state.lng},
      zoom: this.state.zoom,
      mapTypeControl: false
    });

    let infowindow = new window.google.maps.InfoWindow()
    let allMarkers = [];

    //.map returns a new array use forEach instead
    this.state.POIs.forEach(poi => {
      let marker = new window.google.maps.Marker({
        map: map,
        position: {lat: poi.venue.location.lat, lng: poi.venue.location.lng},
        title: poi.venue.name,
        id: poi.venue.id,
        address: poi.venue.location.address,
        city: poi.venue.location.city,
        animation: window.google.maps.Animation.DROP
      })

      let contentString = `${"<b>" + poi.venue.name + "</b> <br />" + poi.venue.location.address + ", <br />" + poi.venue.location.city
      }`

      marker.addListener('click', function(){
        infowindow.setContent(contentString)
        infowindow.open(map,marker)
        toggleBounce(marker)
      })
      allMarkers.push(marker)
    }) // POIs markers forFeach loop

    function toggleBounce(marker) {
      //console.log(marker);
      if (marker.getAnimation() !== null) {
        marker.setAnimation(null);
      } else {
        marker.setAnimation(window.google.maps.Animation.BOUNCE);
        setTimeout(function(){ marker.setAnimation(null); }, 1000);

      }

    }

    //console.log(this.state.open);
    //console.log(toggleList);
    this.setState({infowindow: infowindow });
    this.setState({myMarkers: allMarkers});
    this.setState({myMap:map});
    this.setState({open:this.state.open});
    this.setState({toggleBounce:toggleBounce});

  } //init


  render() {
    return (
      <div className="App">
        <div className="container">
          <div className="">
            <h1>New York City Points Of Interests</h1>
          </div>

            <button onClick={()=>this.toggleList()} className='toggleButton' tabIndex='0'>
              <i className="fa fa-bars"></i>
              <div>Click to search by name</div>
            </button>

          <MenuList
            POIs={this.state.POIs}
            myMap={this.state.myMap}
            infowindow={this.state.infowindow}
            myMarkers={this.state.myMarkers}
            toggleBounce={this.state.toggleBounce}
            open={this.state.open}
            toggleList={this.toggleList}
          />
        </div>
          <div id="map"></div>
      </div>
    )
  }
}

function loadJS(src) {
    var ref = window.document.getElementsByTagName("script")[0];
    var script = window.document.createElement("script");
    script.src = src;
    script.async = true;
    script.defer = true;
    ref.parentNode.insertBefore(script, ref);
}

loadJS.onerror = function() {
  alert("Error: Rendering map!!");
};


export default App;
