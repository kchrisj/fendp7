import React, { Component } from 'react';
import './MenuList.css';
import escapeRegExp from 'escape-string-regexp'
import Drawer from '@material-ui/core/Drawer';


class MenuList extends Component {

  state = {
    query: '',
    open: false,
    filteredPOIs: []
  }

  styles = {
      noBullets: {
        listStyleType: "none",
        padding: 0
      }
  }

  updateQuery = (newQuery) => {
    // save the new query string in state and pass the string
    this.setState({ query: newQuery });
  }

  render(){
    //console.log(this.props.open);
    //console.log(this.props.toggleList);
    //console.log(Drawer);
    const {query} = this.state;
    let showList;
    var myMarkers = this.props.myMarkers;

    if (query) {
      const match = new RegExp(escapeRegExp(query), 'i')
      showList = this.props.myMarkers.filter((list) => match.test(list.title))
      //showList.map(marker => console.log(marker.setVisible(false)))
      filteredMarkers(myMarkers, showList);
      } else {
        showList = this.props.myMarkers;
        filteredMarkers(myMarkers, showList);
    }

    function filteredMarkers(origMarkers, showList) {
      origMarkers.forEach(poi => {
        showList.forEach(marker => {
          if (poi.id !== marker.id){
            poi.setVisible(false);
            marker.visible = true;
          }else{
            marker.setVisible(true);
            poi.setVisible(true);
          }
        })
      })
      if (showList.length === 0){
        origMarkers.forEach(poi => {
          poi.setVisible(false);
        })
      }
    }


    return(
      <div>
        <Drawer open={this.props.open} onClose={()=>this.props.toggleList()} role="search" aria-label="search">
          <div>
            <label htmlFor="query"><strong>Search: </strong></label>
            <input id="query" type="text" placeholder="Type here..." value={this.state.query}
              onChange={(event) => this.updateQuery(event.target.value)}/>
            <ul style={{listStyleType: "none"}} className='poiList'>
              {showList.length > 0 && showList.map((markerName) => (
                <li key={markerName.id} style={{fontWeight: "bold"}}>
                    <button id={markerName.id} className='poibutton' tabIndex="0" aria-label="menu items" onClick={() => markerName.id? [this.props.toggleBounce(markerName), this.props.infowindow.setContent(`${"<b>" + markerName.title + "</b> <br />" + markerName.address + ", <br />" + markerName.city}`), this.props.infowindow.open(this.props.myMap, markerName),markerName.setVisible(true)]:markerName.setVisible(false)
                    }>{markerName.title} </button>
                </li>
              ))}
            </ul>
          </div>
        </Drawer>
      </div>
    );
  }
}
export default MenuList;
